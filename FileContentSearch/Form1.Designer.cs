﻿namespace FileContentSearch
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tb_key = new System.Windows.Forms.TextBox();
            this.btn_search = new System.Windows.Forms.Button();
            this.tb_result = new System.Windows.Forms.TextBox();
            this.tb_path = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_type = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_replace = new System.Windows.Forms.TextBox();
            this.btn_replace = new System.Windows.Forms.Button();
            this.cb_char = new System.Windows.Forms.CheckBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_encode = new System.Windows.Forms.ComboBox();
            this.tb_ct = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_rep = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_num = new System.Windows.Forms.TextBox();
            this.btn_replaceOrder = new System.Windows.Forms.Button();
            this.tip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // tb_key
            // 
            this.tb_key.Location = new System.Drawing.Point(142, 53);
            this.tb_key.Name = "tb_key";
            this.tb_key.Size = new System.Drawing.Size(89, 21);
            this.tb_key.TabIndex = 0;
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(237, 52);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 1;
            this.btn_search.Text = "查  找";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // tb_result
            // 
            this.tb_result.Location = new System.Drawing.Point(12, 100);
            this.tb_result.Multiline = true;
            this.tb_result.Name = "tb_result";
            this.tb_result.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_result.Size = new System.Drawing.Size(502, 274);
            this.tb_result.TabIndex = 2;
            // 
            // tb_path
            // 
            this.tb_path.Location = new System.Drawing.Point(13, 5);
            this.tb_path.Name = "tb_path";
            this.tb_path.ReadOnly = true;
            this.tb_path.Size = new System.Drawing.Size(382, 21);
            this.tb_path.TabIndex = 3;
            this.tb_path.Text = "点击选择目录";
            this.tb_path.Click += new System.EventHandler(this.tb_path_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "文件类型:";
            // 
            // tb_type
            // 
            this.tb_type.Location = new System.Drawing.Point(71, 29);
            this.tb_type.Name = "tb_type";
            this.tb_type.Size = new System.Drawing.Size(443, 21);
            this.tb_type.TabIndex = 6;
            this.tb_type.Text = ".txt|.xml|.as|.cs";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "内容：";
            // 
            // tb_replace
            // 
            this.tb_replace.Location = new System.Drawing.Point(318, 54);
            this.tb_replace.Name = "tb_replace";
            this.tb_replace.Size = new System.Drawing.Size(87, 21);
            this.tb_replace.TabIndex = 8;
            // 
            // btn_replace
            // 
            this.btn_replace.Location = new System.Drawing.Point(409, 53);
            this.btn_replace.Name = "btn_replace";
            this.btn_replace.Size = new System.Drawing.Size(75, 23);
            this.btn_replace.TabIndex = 9;
            this.btn_replace.Text = "替  换";
            this.btn_replace.UseVisualStyleBackColor = true;
            this.btn_replace.Click += new System.EventHandler(this.btn_replace_Click);
            // 
            // cb_char
            // 
            this.cb_char.AutoSize = true;
            this.cb_char.Location = new System.Drawing.Point(14, 56);
            this.cb_char.Name = "cb_char";
            this.cb_char.Size = new System.Drawing.Size(84, 16);
            this.cb_char.TabIndex = 10;
            this.cb_char.Text = "区分大小写";
            this.cb_char.UseVisualStyleBackColor = true;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(12, 381);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(209, 12);
            this.linkLabel1.TabIndex = 11;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "更多工具下载：http://www.56lea.com";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(398, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "编码：";
            // 
            // cb_encode
            // 
            this.cb_encode.FormattingEnabled = true;
            this.cb_encode.Items.AddRange(new object[] {
            "gb2312",
            "UTF-8"});
            this.cb_encode.Location = new System.Drawing.Point(434, 4);
            this.cb_encode.Name = "cb_encode";
            this.cb_encode.Size = new System.Drawing.Size(80, 20);
            this.cb_encode.TabIndex = 13;
            this.cb_encode.Text = "gb2312";
            // 
            // tb_ct
            // 
            this.tb_ct.Location = new System.Drawing.Point(12, 76);
            this.tb_ct.Name = "tb_ct";
            this.tb_ct.Size = new System.Drawing.Size(100, 21);
            this.tb_ct.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(116, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 15;
            this.label4.Text = "替换成";
            // 
            // tb_rep
            // 
            this.tb_rep.Location = new System.Drawing.Point(160, 76);
            this.tb_rep.Name = "tb_rep";
            this.tb_rep.Size = new System.Drawing.Size(100, 21);
            this.tb_rep.TabIndex = 16;
            this.tb_rep.Text = "str$n$str";
            this.tip.SetToolTip(this.tb_rep, "$n$ 为序列数值，例：abc$n$def 即把指定的内容替换成 abc100def,abc101def,abc102def......");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(265, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 18;
            this.label5.Text = "起始值";
            // 
            // tb_num
            // 
            this.tb_num.Location = new System.Drawing.Point(312, 76);
            this.tb_num.Name = "tb_num";
            this.tb_num.Size = new System.Drawing.Size(53, 21);
            this.tb_num.TabIndex = 19;
            this.tb_num.Text = "100";
            this.tb_num.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_num_KeyPress);
            // 
            // btn_replaceOrder
            // 
            this.btn_replaceOrder.Location = new System.Drawing.Point(371, 75);
            this.btn_replaceOrder.Name = "btn_replaceOrder";
            this.btn_replaceOrder.Size = new System.Drawing.Size(75, 23);
            this.btn_replaceOrder.TabIndex = 20;
            this.btn_replaceOrder.Text = "序列替换";
            this.btn_replaceOrder.UseVisualStyleBackColor = true;
            this.btn_replaceOrder.Click += new System.EventHandler(this.btn_replaceOrder_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 403);
            this.Controls.Add(this.btn_replaceOrder);
            this.Controls.Add(this.tb_num);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_rep);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_ct);
            this.Controls.Add(this.cb_encode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.cb_char);
            this.Controls.Add(this.btn_replace);
            this.Controls.Add(this.tb_replace);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_type);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_path);
            this.Controls.Add(this.tb_result);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.tb_key);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "文本查找替换工具 by 56lea.com";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_key;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.TextBox tb_result;
        private System.Windows.Forms.TextBox tb_path;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_type;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_replace;
        private System.Windows.Forms.Button btn_replace;
        private System.Windows.Forms.CheckBox cb_char;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_encode;
        private System.Windows.Forms.TextBox tb_ct;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_rep;
        private System.Windows.Forms.ToolTip tip;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_num;
        private System.Windows.Forms.Button btn_replaceOrder;
    }
}

