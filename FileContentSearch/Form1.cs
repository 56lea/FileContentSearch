﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileContentSearch
{
    public partial class Form1 : Form
    {
        private List<FileInfo> fileListData = new List<FileInfo>(); //保存所有的文件信息 
        public Form1()
        {
            InitializeComponent();
        }
        public void GetAllFilesInDirectory(string strDirectory, Boolean contains)
        {
            if (!Directory.Exists(strDirectory))
            {
                MessageBox.Show("模板路径无效~！");
                return;
            }
            DirectoryInfo directory = new DirectoryInfo(strDirectory);
            DirectoryInfo[] directoryArray = directory.GetDirectories();
            FileInfo[] fileInfoArray = directory.GetFiles();
            if (fileInfoArray.Length > 0) fileListData.AddRange(fileInfoArray);
            if (contains)
            {
                foreach (DirectoryInfo _directoryInfo in directoryArray)
                {//遍历子目录
                    GetAllFilesInDirectory(_directoryInfo.FullName, contains);
                }
            }
        }
        private void btn_search_Click(object sender, EventArgs e)
        {
            tb_result.Text = "包含"+tb_key.Text+"的文件有：\r\n";
            string key=tb_key.Text;
            foreach (FileInfo fi in fileListData)
            {
                if (checkFile(fi.Name))
                {
                    string c = File.ReadAllText(fi.FullName, Encoding.GetEncoding(cb_encode.Text));
                    if (!cb_char.Checked)
                    {
                        key = key.ToLower();
                        c = c.ToLower();
                    }
                    if (c.IndexOf(key) != -1)
                    {
                        tb_result.Text += fi.FullName + "\r\n";
                    }
                }
            }
        }

        private Boolean checkFile(string f)
        {
            if (tb_type.Text.Length < 1) return true;
            string[] type = tb_type.Text.Split('|');
            foreach(string i in type){
                if (f.IndexOf(i) != -1) return true;
            }
            return false;
        }

        private void tb_path_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fb = new FolderBrowserDialog();
            if (fb.ShowDialog() == DialogResult.OK)
            {
                tb_path.Text = fb.SelectedPath;
                GetAllFilesInDirectory(tb_path.Text, true);
            }
        }

        private void btn_replace_Click(object sender, EventArgs e)
        {
            replace(tb_key.Text, tb_replace.Text);
        }
        private void replace(string str1, string str2)
        {
            tb_result.Text = "下列文件已被替换：\r\n";
            foreach (FileInfo fi in fileListData)
            {
                if (checkFile(fi.Name))
                {
                    string c = File.ReadAllText(fi.FullName, Encoding.GetEncoding(cb_encode.Text));
                    if (!cb_char.Checked)
                    {
                        str1 = str1.ToLower();
                        c = c.ToLower();
                    }
                    if (c.IndexOf(str1) != -1)
                    {
                        c = c.Replace(str1, str2);
                        File.WriteAllText(fi.FullName, c, Encoding.GetEncoding(cb_encode.Text));
                        tb_result.Text += fi.FullName + "\r\n";
                    }
                }
            }
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.56lea.com");
        }

        private void btn_replaceOrder_Click(object sender, EventArgs e)
        {
            if (tb_ct.Text.Trim().Length < 1)
            {
                MessageBox.Show("替换源内容不能为空");
                return;
            }
            if (tb_rep.Text.Trim().Length < 1)
            {
                MessageBox.Show("替换内容不能为空！");
                return;
            }
            if (tb_rep.Text.IndexOf("$n$") == -1)
            {
                replace(tb_ct.Text, tb_rep.Text);
            }
            else
            {
                string str1 = tb_ct.Text;
                int n = Convert.ToInt32(tb_num.Text);
                tb_result.Text = "下列文件已被替换：\r\n";
                foreach (FileInfo fi in fileListData)
                {
                    if (checkFile(fi.Name))
                    {
                        string c = File.ReadAllText(fi.FullName, Encoding.GetEncoding(cb_encode.Text));
                        if (!cb_char.Checked)
                        {
                            str1 = str1.ToLower();
                            c = c.ToLower();
                        }
                        if (c.IndexOf(str1) != -1)
                        {
                            while (c.IndexOf(str1) != -1)
                            {
                                c = c.Substring(0, c.IndexOf(str1)) + tb_rep.Text.Replace("$n$", n.ToString()) + c.Substring(c.IndexOf(str1) + str1.Length);
                                n++;
                                //c.Replace(str1, tb_rep.Text.Replace("$n$",n.ToString()));
                            }
                            File.WriteAllText(fi.FullName, c, Encoding.GetEncoding(cb_encode.Text));
                            tb_result.Text += fi.FullName + "\r\n";
                        }
                    }
                }
            }
        }

        private void tb_num_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
